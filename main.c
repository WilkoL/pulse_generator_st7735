#define X_SIZE 160
#define Y_SIZE 128


#include "main.h"

extern volatile uint8_t button;
extern volatile int8_t encoder;


int main(void)
{
	char buf_freq[8];
	char buf_freq_unit[4];
	char buf_puls[8];
	char buf_puls_unit[4];
	char buf_maxpuls[8];
	char buf_maxpuls_unit[4];
	
	uint8_t freq_table;
	uint8_t freq_table_mod9;
	uint32_t frequency_tft;
	uint8_t psc_tim2;
	uint16_t arr_tim2;
	uint16_t ccr_tim2;
	
	uint8_t pulse_table;
	uint32_t pulsewidth_timer;
	uint32_t pulsewidth_tft;
	uint16_t psc_tim1;
	uint16_t arr_tim1;
	
	uint8_t pulse_table_maximum;
	uint32_t pulsewidth_maximum_tft;
	uint16_t psc_tim1_max;
	uint16_t arr_tim1_max;
	
	uint8_t first_run;
	uint8_t display_freq_flag;
	uint8_t display_puls_flag;
	uint8_t previous_button;
	
	
	
	CLK_HSE_Config();
	GPIO_Config();
	EXTI_Config();
	
	TIM1_Config();
	TIM2_Config();
	TIM4_Config();
	
	ST7735_Init();
	ST7735_AddrSet(0,0,X_SIZE,Y_SIZE);
	ST7735_Clear(COLOR565_BLACK);
	ST7735_Orientation(scr_CW);
	
	ST7735_PutStr5x7(2, 15, 10,"Pulse", COLOR565_WHITE, COLOR565_BLACK);
	ST7735_PutStr5x7(2, 15, 35,"Generator", COLOR565_WHITE, COLOR565_BLACK);
	DELAY_ms(2000);
	ST7735_Clear(COLOR565_BLACK);
	ST7735_PutStr5x7(2, 15, 10, "Freq", COLOR565_WHITE, COLOR565_BLACK);
	ST7735_PutStr5x7(2, 15, 35, "Pulse", COLOR565_WHITE, COLOR565_BLACK);
	ST7735_PutStr5x7(1, 15, 70, "Max. pulse length" , COLOR565_WHITE, COLOR565_BLACK);

	//default settings
	freq_table = 35;
	pulse_table = 26;
	pulse_table_maximum = 34;
	
	display_freq_flag = 1;
	display_puls_flag = 1;
	previous_button = 1;
	first_run = 1;
	
	
	while (1)
	{
		if ((encoder) || (first_run == 1))
		{
			if ((button == 0) || (first_run == 1))											//FREQUENCY SETTING
			{
				if (freq_table < 7) freq_table = 7;
				if (freq_table > 80) freq_table = 80;
				
				if ((encoder == -1) && (freq_table > 7)) freq_table--;						//don't go higher than 2 MHz
				if ((encoder == 1) && (freq_table < 80)) freq_table++;						//lowest (10 mHz)
				
				psc_tim2 = freq_prescaler[(uint8_t) (freq_table / 9)];						//9 possible prescalers
				arr_tim2 = freq_divider[freq_table];										//81 possible dividers (not all are used)
				ccr_tim2 = freq_divider[freq_table] / 2;									//50% duty cycle
				
				TIM2->PSCR = psc_tim2;
				TIM2->ARRH = (uint8_t) (arr_tim2 / 256);
				TIM2->ARRL = (uint8_t) (arr_tim2 % 256);
				
				TIM2->CCR1H = (uint8_t) (ccr_tim2 / 256);
				TIM2->CCR1L = (uint8_t) (ccr_tim2 % 256);
				
				TIM2->CNTRH = 0x00;
				TIM2->CNTRL = 0x00;
				
				TIM2->EGR |= TIM1_EGR_UG;	//reset timer
				
				
				//calculate display frequency numbers and units
				frequency_tft = (uint32_t) 1600000000UL / (1UL << psc_tim2) / (uint32_t) (arr_tim2 + 1);
				
				if (frequency_tft < 100) 
				{						
					sprintf(buf_freq, "%ld", (frequency_tft * 10));
					sprintf(buf_freq_unit, "mHz");
				}
				else if (frequency_tft < 100000) 
				{
					sprintf(buf_freq, "%ld", (frequency_tft / 100));
					sprintf(buf_freq_unit, "Hz ");
				}
				else if (frequency_tft < 100000000)
				{
					sprintf(buf_freq, "%ld", (frequency_tft / 100000));
					sprintf(buf_freq_unit, "kHz");
				}
				else
				{
					sprintf(buf_freq, "%ld", (frequency_tft / 100000000));
					sprintf(buf_freq_unit, "MHz");
				}
				
				//max pulse length is dependend on the frequency, here the max length is selected
				freq_table_mod9 = (uint8_t) (freq_table % 9);
				switch (freq_table_mod9)
				{
				case 0:
				case 8:
					pulse_table_maximum = (uint8_t) (freq_table - 1);
					break;
					
				case 1:
					pulse_table_maximum = (uint8_t) (freq_table - 2);
					break;
					
				case 2:
					pulse_table_maximum = (uint8_t) (freq_table - 3);
					break;
					
				case 3:
					pulse_table_maximum = (uint8_t) (freq_table - 4);
					break;
					
				case 4:
				case 5:
				case 6:
				case 7:
					pulse_table_maximum = (uint8_t) (freq_table - 5);
					break;
					
				default:
					pulse_table_maximum = 7;
					break;
				}
				
				if (pulse_table > pulse_table_maximum)		//if the pulse length exceeds the max possible length
				{																					//it is reduced to the max and a signal to calculate
					pulse_table = pulse_table_maximum;			//the "new" length is generated with a "fake" 
					button = 1;															//button-press
					encoder = 0;
				}
				
				//calculate the *display values* of the max pulse length
				psc_tim1_max = pulsewidth_prescaler[pulse_table_maximum / 9];
				arr_tim1_max = pulsewidth_divider[pulse_table_maximum];
				
				pulsewidth_timer = (uint32_t) (psc_tim1_max + 1) * (uint32_t) (arr_tim1_max + 1);
				
				if ((pulse_table_maximum == 0) || ((pulse_table_maximum >= 1) && (pulse_table_maximum < 8)))
				{
					pulsewidth_tft = (pulsewidth_timer * 1000 / 16) + 63;
					sprintf(buf_maxpuls, "%ld", pulsewidth_tft);
					sprintf(buf_maxpuls_unit, "ns");
				}
				else if ((pulse_table_maximum >= 8) && (pulse_table_maximum < 35)) 
				{
					pulsewidth_tft = pulsewidth_timer / 16;
					sprintf(buf_maxpuls, "%ld", pulsewidth_tft);
					sprintf(buf_maxpuls_unit, "us");
				}
				else if ((pulse_table_maximum >= 35) && (pulse_table_maximum < 62))
				{
					pulsewidth_tft = pulsewidth_timer /16000;
					sprintf(buf_maxpuls, "%ld", pulsewidth_tft);
					sprintf(buf_maxpuls_unit, "ms");
				}
				else
				{
					pulsewidth_tft = pulsewidth_timer /16000000;
					sprintf(buf_maxpuls, "%ld", pulsewidth_tft);
					sprintf(buf_maxpuls_unit, "s ");
				}
				
				encoder = 0;
				display_freq_flag = 1;
			}
			
			
			//PULSEWIDTH SETTING
			if ((button == 1) || (first_run == 1))	
			{
				if (pulse_table > 79) pulse_table = 79;
				
				if ((encoder == -1) && (pulse_table < 79)) pulse_table++;
				if ((encoder == 1) && (pulse_table > 0)) pulse_table--;
				
				if (pulse_table > pulse_table_maximum) pulse_table = pulse_table_maximum;

				psc_tim1 = pulsewidth_prescaler[pulse_table / 9];
				arr_tim1 = pulsewidth_divider[pulse_table];
				
				TIM1->PSCRH = (uint8_t) (psc_tim1 / 256);													//new prescaler
				TIM1->PSCRL = (uint8_t) (psc_tim1 % 256);
				
				TIM1->ARRH = (uint8_t) (arr_tim1 / 256);													//new max counter
				TIM1->ARRL = (uint8_t) (arr_tim1 % 256);
				
				TIM1->CNTRH = 0x00;																								//set counter to 0
				TIM1->CNTRL = 0x00;
				
				TIM1->EGR |= TIM1_EGR_UG;																					//reset timer
				
				
				//calculate display puls- numbers and units
				pulsewidth_timer = (uint32_t) (psc_tim1 + 1) * (uint32_t) (arr_tim1 + 1);
				
				if ((pulse_table == 0) || ((pulse_table >= 1) && (pulse_table < 8)))
				{
					pulsewidth_tft = (pulsewidth_timer * 1000 / 16) + 63;					//63ns correction
					sprintf(buf_puls, "%ld", pulsewidth_tft);
					sprintf(buf_puls_unit, "ns");
				}
				else if ((pulse_table >= 8) && (pulse_table < 35))
				{
					pulsewidth_tft = pulsewidth_timer / 16;
					sprintf(buf_puls, "%ld", pulsewidth_tft);
					sprintf(buf_puls_unit, "us");
				}
				else if ((pulse_table >= 35) && (pulse_table < 62))
				{
					pulsewidth_tft = pulsewidth_timer /16000;
					sprintf(buf_puls, "%ld", pulsewidth_tft);
					sprintf(buf_puls_unit, "ms");
				}
				else
				{
					pulsewidth_tft = pulsewidth_timer /16000000;
					sprintf(buf_puls, "%ld", pulsewidth_tft);
					sprintf(buf_puls_unit, "s ");
				}
				encoder = 0;
				display_puls_flag = 1;
			}
			first_run = 0;
		}
		
		
		//DISPLAY THE SELECTION OF FREQ OR PULSE
		if ((button == 0) && (previous_button == 1))
		{
			ST7735_PutStr5x7(2, 0, 10, ">", COLOR565_WHITE, COLOR565_BLACK);
			ST7735_PutStr5x7(2, 0, 35, " ", COLOR565_WHITE, COLOR565_BLACK);
			previous_button = 0;
		}
		
		if ((button == 1) && (previous_button == 0))
		{
			ST7735_PutStr5x7(2, 0, 10, " ", COLOR565_WHITE, COLOR565_BLACK);
			ST7735_PutStr5x7(2, 0, 35, ">", COLOR565_WHITE, COLOR565_BLACK);
			previous_button = 1;
		}


		//DISPLAY FREQUENCY & MAX_PULSEWIDTH
		if (display_freq_flag == 1)
		{
			ST7735_PutStr5x7(2, 80, 10, "   ",          COLOR565_WHITE, COLOR565_BLACK);
			ST7735_PutStr5x7(2, 80, 10,  buf_freq,      COLOR565_WHITE, COLOR565_BLACK);
			ST7735_PutStr5x7(2, 125, 10, buf_freq_unit, COLOR565_WHITE, COLOR565_BLACK);

			ST7735_PutStr5x7(2, 80, 90, "   ",             COLOR565_WHITE, COLOR565_BLACK);
			ST7735_PutStr5x7(2, 80, 90,  buf_maxpuls,      COLOR565_WHITE, COLOR565_BLACK);
			ST7735_PutStr5x7(2, 125, 90, buf_maxpuls_unit, COLOR565_WHITE, COLOR565_BLACK);
			
			display_freq_flag = 0;
		}
		
		
		//DISPLAY PULSEWIDTH
		if (display_puls_flag == 1)
		{
			ST7735_PutStr5x7(2, 80, 35, "   ", COLOR565_WHITE, COLOR565_BLACK);
			ST7735_PutStr5x7(2, 80, 35, buf_puls, COLOR565_WHITE, COLOR565_BLACK);
			ST7735_PutStr5x7(2, 125, 35, buf_puls_unit, COLOR565_WHITE, COLOR565_BLACK);
			
			display_puls_flag = 0;
		}
	}
}



#ifdef USE_FULL_ASSERT

void assert_failed(uint8_t* file, uint32_t line)
{
	while (1)
	{
	}
}
#endif