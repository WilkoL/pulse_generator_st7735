#ifndef __TABLES_H__
#define __TABLES_H__

#include "stm8s.h"

extern const uint16_t freq_divider[81];
extern const uint8_t freq_prescaler[9];

extern const uint16_t pulsewidth_divider[81];
extern const uint16_t pulsewidth_prescaler[9];

#endif
