#ifndef __MAIN_H__
#define __MAIN_H__

#include <stm8s.h>
#include <stdio.h>

//#include "stm8s_gpio.h"

#include "stm8_clk_hse.h"
#include "stm8_gpio.h"
#include "stm8_exti.h"

#include "stm8_tim1.h"
#include "stm8_tim2.h"
#include "stm8_tim4.h"

#include "st7735.h"
#include "tables.h"


#endif