#ifndef __ST7735_H__
#define __ST7735_H__

#include "main.h"
#include <string.h>
#include "font5x7.h"
//#include "font7x11.h"
#include "color565.h"

// Screen resolution in normal orientation
#define scr_w         128
#define scr_h         160

//here we use SPI1

#define SPI_PORT		GPIOD
#define ST7735_PORT	GPIOD

#define ST7735_RST	GPIO_PIN_6
#define ST7735_A0		GPIO_PIN_5
#define SPI_CS			GPIO_PIN_4
#define SPI_MOSI		GPIO_PIN_3
#define SPI_CLK			GPIO_PIN_2
//#define SPI_MISO	LL_GPIO_PIN_6


// CS pin macros
#define CS_L() SPI_PORT->ODR &= (uint8_t) ~SPI_CS
#define CS_H() SPI_PORT->ODR |= (uint8_t) SPI_CS

// A0 pin macros
#define A0_L() ST7735_PORT->ODR &= (uint8_t) ~ST7735_A0
#define A0_H() ST7735_PORT->ODR |= (uint8_t) ST7735_A0

// RESET pin macros
#define RST_L() ST7735_PORT->ODR &= (uint8_t) ~ST7735_RST
#define RST_H() ST7735_PORT->ODR |= (uint8_t) ST7735_RST


typedef enum
{
  scr_normal = 0,
  scr_CW     = 1,
  scr_CCW    = 2,
  scr_180    = 3
} ScrOrientation_TypeDef;

extern uint16_t scr_width;
extern uint16_t scr_height;

uint16_t RGB565(uint8_t R,uint8_t G,uint8_t B);

//public prototypes
void ST7735_Init(void);
void ST7735_AddrSet(uint16_t XS, uint16_t YS, uint16_t XE, uint16_t YE);
void ST7735_Orientation(uint8_t orientation);
void ST7735_Clear(uint16_t color);
void ST7735_Pixel(uint16_t X, uint16_t Y, uint16_t color);
void ST7735_HLine(uint16_t X1, uint16_t X2, uint16_t Y, uint16_t color);
void ST7735_VLine(uint16_t X, uint16_t Y1, uint16_t Y2, uint16_t color);
void ST7735_Line(int16_t X1, int16_t Y1, int16_t X2, int16_t Y2, uint16_t color);
void ST7735_Rect(uint16_t X1, uint16_t Y1, uint16_t X2, uint16_t Y2, uint16_t color);
void ST7735_FillRect(uint16_t X1, uint16_t Y1, uint16_t X2, uint16_t Y2, uint16_t color);
void ST7735_PutChar5x7(uint8_t scale, uint16_t X, uint16_t Y, uint8_t chr, uint16_t color, uint16_t bgcolor);
void ST7735_PutStr5x7(uint8_t scale, uint8_t X, uint8_t Y, char *str, uint16_t color, uint16_t bgcolor);
//void ST7735_PutChar7x11(uint16_t X, uint16_t Y, uint8_t chr, uint16_t color, uint16_t bgcolor);
//void ST7735_PutStr7x11(uint8_t X, uint8_t Y, char *str, uint16_t color, uint16_t bgcolor);


#endif
