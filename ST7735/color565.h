#ifndef __COLOR565_H__
#define __COLOR565_H__


#define COLOR565_BLACK                      0x0000
#define COLOR565_BLUE                       0x001F
#define COLOR565_DARK_BLUE                  0x0011
#define COLOR565_CYAN                       0x07FF
#define COLOR565_LIGHT_GRAY                 0xD69A
#define COLOR565_GRAY                       0x8410
#define COLOR565_GREEN                      0x0400
#define COLOR565_MAGENTA                    0xF81F
#define COLOR565_ORANGE                     0xFD20
#define COLOR565_PURPLE                     0x8010
#define COLOR565_RED                        0xF800
#define COLOR565_WHITE                      0xFFFF
#define COLOR565_YELLOW                     0xFFE0

#endif
