/*
* settings of prescaler and ARR asumes a masterclock of 16 MHz
* generates a TIM4_UPDATE interrupt every 1 ms
* needs handling of the interrupt in stm8s_it.s
*/


#include "stm8_tim4.h"

volatile uint8_t countflag;

void TIM4_Config(void)
{
	disableInterrupts();
	
	CLK->PCKENR1 |= CLK_PCKENR1_TIM4;				//enable TIMER4 clock
	
	TIM4->CR1 = (TIM4_CR1_ARPE | TIM4_CR1_CEN);		//TIM4_ARR register is buffered, TIM4 enabled
	TIM4->IER = TIM4_IER_UIE;								//update interrupt enabled
	TIM4->EGR = 0x00;												//no events generated
	TIM4->PSCR = 0x07;											//prescaler = 128	//125 kHz					
	TIM4->ARR = 124;												//count to 125		//1 kHz    (1 ms)
	
	enableInterrupts();
}

void DELAY_ms(uint32_t delay_ms)					//function that waits for "delay_ms" milliseconds
{
	volatile uint32_t milliseconds;
	
	milliseconds = delay_ms;
	while (milliseconds)
	{
		if (countflag)												//countflag is set every ms
		{
			countflag = 0;
			milliseconds--;
		}
	}
}

void DELAY_us(uint32_t delay_us)					//function that waits for approx "delay_us" microseconds
{
	volatile uint32_t microseconds;
	microseconds = delay_us / USFACTOR;
	
	while (microseconds-- > 0);
}