#include "stm8_clk_hse.h"

void CLK_HSE_DeConfig(void)
{
	disableInterrupts();
	
	CLK->ICKR = 0x01;
	CLK->ECKR = 0x00;
	CLK->CMSR = 0xE1;
	CLK->SWR  = 0xE1;
	CLK->SWCR = 0x00;
	CLK->CKDIVR = 0x18;

	CLK->PCKENR1 = 0x00;							//disable all peripheral clocks
	CLK->PCKENR2 = 0x00;							//disable all peripheral clocks
	
	CLK->CSSR = 0x00;
	CLK->CCOR = 0x00;
	while ((CLK->CCOR & CLK_CCOR_CCOEN)!= 0);
	CLK->CCOR = 0x00;
	CLK->HSITRIMR = 0x00;
	CLK->SWIMCCR = 0x00;
	
	enableInterrupts();
}

void CLK_HSE_Config(void)
{
	disableInterrupts();
	
	CLK->ECKR = CLK_ECKR_HSEEN;						//enable HSE
	while (!(CLK->ECKR & CLK_ECKR_HSERDY));			//wacht tot HSE is ready
	CLK->SWCR = CLK_SWCR_SWEN;						//enable clock switch
	CLK->SWR = 0xB4;								//select HSE as master clock
	while (CLK->SWCR & CLK_SWCR_SWBSY);				//wait for switch to complete
	CLK->CKDIVR = 0x00;								//fcpu=fhsi/1, fcpu=fmaster/1

	CLK->CSSR = CLK_CSSR_CSSEN;						//clock security system = on
	//CLK->CCOR = (CLK_CCOR_CCOEN | (6 << 1));		//Clock output enable, fcpu/4
	CLK->HSITRIMR = 0x00;							//no HSI clock trim
	CLK->SWIMCCR = 0x00;							//swim clock /2
	
	enableInterrupts();
}

