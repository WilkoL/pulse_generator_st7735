#ifndef STM8_TIM4_H
#define STM8_TIM4_H

/*
* USFACTOR depends on fcpu, to get to approx. correct micro-seconds
* with a 16 MHz clock the number of us need to be DIVIDED by a factor
* of 4 to get a reasonable us timer. It works quite well from about
* 100 us (and higher)
*/

#include "stm8s.h"
#define USFACTOR 4

extern volatile uint8_t countflag;

void TIM4_Config(void);
void DELAY_ms(uint32_t);
void DELAY_us(uint32_t);


#endif
