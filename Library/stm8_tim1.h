#ifndef STM8_TIM1_H
#define STM8_TIM1_H

#include "stm8s.h"
//#include "stm8s_tim1.h"

void TIM1_DeConfig(void);
void TIM1_Config(void);

#endif
