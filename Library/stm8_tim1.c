#include "stm8_tim1.h"

void TIM1_DeConfig(void)
{
  TIM1->CR1  = 0x00;
  TIM1->CR2  = 0x00;
  TIM1->SMCR = 0x00;
  TIM1->ETR  = 0x00;
  TIM1->IER  = 0x00;
  TIM1->SR2  = 0x00;
  /* Disable channels */
  TIM1->CCER1 = 0x00;
  TIM1->CCER2 = 0x00;
  /* Configure channels as inputs: it is necessary if lock level is equal to 2 or 3 */
  TIM1->CCMR1 = 0x01;
  TIM1->CCMR2 = 0x01;
  TIM1->CCMR3 = 0x01;
  TIM1->CCMR4 = 0x01;
  /* Then reset channel registers: it also works if lock level is equal to 2 or 3 */
  TIM1->CCER1 = 0x00;
  TIM1->CCER2 = 0x00;
  TIM1->CCMR1 = 0x00;
  TIM1->CCMR2 = 0x00;
  TIM1->CCMR3 = 0x00;
  TIM1->CCMR4 = 0x00;
  TIM1->CNTRH = 0x00;
  TIM1->CNTRL = 0x00;
  TIM1->PSCRH = 0x00;
  TIM1->PSCRL = 0x00;
  TIM1->ARRH  = 0xFF;
  TIM1->ARRL  = 0xFF;
  TIM1->CCR1H = 0x00;
  TIM1->CCR1L = 0x00;
  TIM1->CCR2H = 0x00;
  TIM1->CCR2L = 0x00;
  TIM1->CCR3H = 0x00;
  TIM1->CCR3L = 0x00;
  TIM1->CCR4H = 0x00;
  TIM1->CCR4L = 0x00;
  TIM1->OISR  = 0x00;
  TIM1->EGR   = 0x01; // TIM1_EGR_UG
  TIM1->DTR   = 0x00;
  TIM1->BKR   = 0x00;
  TIM1->RCR   = 0x00;
  TIM1->SR1   = 0x00;
}


void TIM1_Config(void)
{
	disableInterrupts();
	
	CLK->PCKENR1 |= CLK_PCKENR1_TIM1;		//enable TIMER1 clock
		
	TIM1_DeConfig();
	
	TIM1->CR1 |= TIM1_CR1_ARPE; 			//ARR buffered,
	TIM1->CR1 &= (uint8_t) ~TIM1_CR1_CEN;		//NOT YET enabled
	TIM1->CR1 |= TIM1_CR1_OPM;				//One Pulse Mode
	
	TIM1->CR2 = 0x00;									//geen TRGO
		
	TIM1->SMCR |= 0x50;								//triggering input from TI1FP1
	TIM1->SMCR |= 0x06;								//slave mode: Trigger standard mode
	TIM1->ETR = 0x00;									//geen externe clock
		
	TIM1->IER = 0x00;									//geen interrupts enabled
	TIM1->EGR = 0x00;									//geen self-triggering events generated
		
	TIM1->CCMR1 = 0x00;								//geen output compare mode
	TIM1->CCMR2 = 0x00;
	TIM1->CCMR3 = 0x00;
	
	
	TIM1->CCMR4 &= (uint8_t) ~TIM1_CCMR_CCxS;		//CC4 is output
	TIM1->CCMR4 |= TIM1_CCMR_OCxFE;		//fast enable
	TIM1->CCMR4 |= 0x70;							//PWM mode
		
	TIM1->CCER1 = 0x00;								//geen in/output capture/compare mode CH1 CH2
	TIM1->CCER2 |= TIM1_CCER2_CC4E;		//OC4 connected to CH4 output pin
	
	TIM1->PSCRH = (15 / 256);					//16MHz / 16 = 1 MHz (1 us)
	TIM1->PSCRL = (15 % 256);

	TIM1->ARRH = (9 / 256);						//10 x 1us = 1000us = 10 us
	TIM1->ARRL = (9 % 256);
		
	TIM1->RCR = 0x00;									//repetition counter value
	
	TIM1->CCR1H = 0x00;
	TIM1->CCR1L = 0x00;

	TIM1->CCR2H = 0x00;
	TIM1->CCR2L = 0x00;
	
	TIM1->CCR3H = 0x00;
	TIM1->CCR3L = 0x00;
	
	TIM1->CCR4H = 0x00;								//compare value CH4
	TIM1->CCR4L = 0x01;								//overruled by "fast enable"

	TIM1->BKR = TIM1_BKR_MOE;					//master output ON, geen break
	TIM1->DTR = 0x00;									//deadtime register
	TIM1->OISR = 0x00;								//output idle state
	
	TIM1->CR1 |= TIM1_CR1_CEN;				//TIM1 enabled
	
	enableInterrupts();
}