#include "stm8_exti.h"


void EXTI_Config(void)
{
	disableInterrupts();
	
	EXTI->CR1 |= (2 << 0);							//enable EXTI on falling edge of PORTA
	EXTI->CR1 |= (2 << 2);							//enable EXTI on falling edge of PORTB
	EXTI->CR2 = 0;

	enableInterrupts();
}