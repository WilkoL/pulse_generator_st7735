#include "stm8_tim2.h"

void TIM2_DeConfig(void)
{
  TIM2->CR1 = (uint8_t)0x00;
  TIM2->IER = (uint8_t)0x00;
  TIM2->SR2 = (uint8_t)0x00;
  
  /* Disable channels */
  TIM2->CCER1 = (uint8_t)0x00;
  TIM2->CCER2 = (uint8_t)0x00;
  
  
  /* Then reset channel registers: it also works if lock level is equal to 2 or 3 */
  TIM2->CCER1 = (uint8_t)0x00;
  TIM2->CCER2 = (uint8_t)0x00;
  TIM2->CCMR1 = (uint8_t)0x00;
  TIM2->CCMR2 = (uint8_t)0x00;
  TIM2->CCMR3 = (uint8_t)0x00;
  TIM2->CNTRH = (uint8_t)0x00;
  TIM2->CNTRL = (uint8_t)0x00;
  TIM2->PSCR = (uint8_t)0x00;
  TIM2->ARRH  = (uint8_t)0xFF;
  TIM2->ARRL  = (uint8_t)0xFF;
  TIM2->CCR1H = (uint8_t)0x00;
  TIM2->CCR1L = (uint8_t)0x00;
  TIM2->CCR2H = (uint8_t)0x00;
  TIM2->CCR2L = (uint8_t)0x00;
  TIM2->CCR3H = (uint8_t)0x00;
  TIM2->CCR3L = (uint8_t)0x00;
  TIM2->SR1 = (uint8_t)0x00;
}
  

void TIM2_Config(void)
{
	disableInterrupts();
	
	CLK->PCKENR1 |= CLK_PCKENR1_TIM2;		//enable TIMER2 clock
	
	TIM2_DeConfig();
	
	TIM2->CR1 |= TIM2_CR1_ARPE;					//ARR register buffered through a preload register

	TIM2->IER = 0x00;
	TIM2->EGR = 0x00;
	
	TIM2->CCMR1 |= (6<<4);							//OC1M = 110 : PWM mode 1
	TIM2->CCMR1 |= TIM2_CCMR_OCxPE;			//Preload register on TIMx_CCR1 enabled
	TIM2->CCMR1 |= 0;										//CC1S = 00 : CC1 channel is configured as output
	TIM2->CCMR2 = 0x00;
	TIM2->CCMR3 = 0x00;

	TIM2->CCER1 |= TIM2_CCER1_CC1E;			//CC1 output enable
	TIM2->CCER2 = 0x00;
	
	TIM2->CNTRH = 0x00;									//set counter to 0
	TIM2->CNTRL = 0x00;
	//																	//system clock    (16 MHz)
	TIM2->PSCR = 2;											//prescaler = 4   (4 MHz)
	
	TIM2->ARRH = (3999 / 256);					//ARR = 49999 --> 10 Hz
	TIM2->ARRL = (3999 % 256);
	
	TIM2->CCR1H = (1999 / 256);					//compare1 = ARR / 2
	TIM2->CCR1L = (1999 % 256);

	TIM2->CR1 |= TIM2_CR1_CEN;					//counter enabled
		
	enableInterrupts();
}
