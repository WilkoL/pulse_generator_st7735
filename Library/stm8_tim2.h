#ifndef STM8_TIM2_H
#define STM8_TIM2_H

#include "stm8s.h"

void TIM2_DeConfig(void);
void TIM2_Config(void);

#endif
