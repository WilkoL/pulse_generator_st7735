#ifndef STM8_GPIO_H
#define STM8_GPIO_H

#include "stm8s.h"

typedef enum
{
  GPIO_PIN_0    = ((uint8_t)0x01),
  GPIO_PIN_1    = ((uint8_t)0x02),
  GPIO_PIN_2    = ((uint8_t)0x04),
  GPIO_PIN_3    = ((uint8_t)0x08),
  GPIO_PIN_4    = ((uint8_t)0x10),
  GPIO_PIN_5    = ((uint8_t)0x20),
  GPIO_PIN_6    = ((uint8_t)0x40),
  GPIO_PIN_7    = ((uint8_t)0x80),
  GPIO_PIN_LNIB = ((uint8_t)0x0F),  // Low nibble pins
  GPIO_PIN_HNIB = ((uint8_t)0xF0),  // High nibble pins
  GPIO_PIN_ALL  = ((uint8_t)0xFF)   // All pins
} GPIO_Pin_TypeDef;


void GPIO_DeConfig(void);
void GPIO_Config(void);

#endif
