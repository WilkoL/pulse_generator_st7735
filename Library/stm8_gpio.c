/*
*	I2C ports SDA and SCL are ALWAYS open-drain
*	and do NOT have pullup resistors
*	(PB4 PB5)
*/

#include "stm8_gpio.h"

void GPIO_DeConfig(void)
{
	GPIOA->ODR = (uint8_t) ~GPIO_PIN_ALL;			//reset all GPIO to defaults
	GPIOA->DDR = (uint8_t) ~GPIO_PIN_ALL;
	GPIOA->CR1 = (uint8_t) ~GPIO_PIN_ALL;
	GPIOA->CR2 = (uint8_t) ~GPIO_PIN_ALL;
	
	GPIOB->ODR = (uint8_t) ~GPIO_PIN_ALL;
	GPIOB->DDR = (uint8_t) ~GPIO_PIN_ALL;
	GPIOB->CR1 = (uint8_t) ~GPIO_PIN_ALL;
	GPIOB->CR2 = (uint8_t) ~GPIO_PIN_ALL;
	
	GPIOC->ODR = (uint8_t) ~GPIO_PIN_ALL;
	GPIOC->DDR = (uint8_t) ~GPIO_PIN_ALL;
	GPIOC->CR1 = (uint8_t) ~GPIO_PIN_ALL;
	GPIOC->CR2 = (uint8_t) ~GPIO_PIN_ALL;
	
	GPIOD->ODR = (uint8_t) ~GPIO_PIN_ALL;
	GPIOD->DDR = (uint8_t) ~GPIO_PIN_ALL;
	GPIOD->CR1 = (uint8_t) ~GPIO_PIN_ALL;
	GPIOD->CR2 = (uint8_t) ~GPIO_PIN_ALL;
}

void GPIO_Config(void)
{
	//ROTARY_KNOB (input + exti)
	GPIOA->ODR &= (uint8_t) ~GPIO_PIN_3;			//PA3 = input
	GPIOA->DDR &= (uint8_t) ~GPIO_PIN_3;			//PA3 = input
	GPIOA->CR1 |= (uint8_t)  GPIO_PIN_3;			//PA3 = input with pullup
	GPIOA->CR2 |= (uint8_t)  GPIO_PIN_3;			//PA3 = input with exti
	
	//ROTARY_B (input + exti)
	GPIOB->ODR &= (uint8_t) ~GPIO_PIN_5;			//PB5 = input
	GPIOB->DDR &= (uint8_t) ~GPIO_PIN_5;			//PB5 = input
	GPIOB->CR1 |= (uint8_t)  GPIO_PIN_5;			//PB5 = input with pullup
	GPIOB->CR2 |= (uint8_t)  GPIO_PIN_5;			//PB5 = input with exti
	
	//ROTARY_A (input)
	GPIOB->ODR &= (uint8_t) ~GPIO_PIN_4;			//PB4 = input
	GPIOB->DDR &= (uint8_t) ~GPIO_PIN_4;			//PB4 = input
	GPIOB->CR1 |= (uint8_t)  GPIO_PIN_4;			//PB4 = input with pullup
	GPIOB->CR2 &= (uint8_t) ~GPIO_PIN_4;			//PB4 = input without exti
	
	
	
	
	//TIM2_CH1 (output)
	GPIOC->ODR &= (uint8_t) ~GPIO_PIN_5;			//PC5 = output low
	GPIOC->DDR |= (uint8_t)  GPIO_PIN_5;			//PC5 = output
	GPIOC->CR1 |= (uint8_t)  GPIO_PIN_5;			//PC5 = output push pull
	GPIOC->CR2 |= (uint8_t)  GPIO_PIN_5;			//PC5 = output high speed
	
	//TIM1_CH1 (input)
	GPIOC->ODR &= (uint8_t) ~GPIO_PIN_6;			//PC6 = input
	GPIOC->DDR &= (uint8_t) ~GPIO_PIN_6;			//PC6 = input
	GPIOC->CR1 |= (uint8_t)  GPIO_PIN_6;			//PC6 = input with pullup
	GPIOC->CR2 &= (uint8_t) ~GPIO_PIN_6;			//PC6 = input without exti
	
	//TIM1_CH4 (output)
	GPIOC->ODR &= (uint8_t) ~GPIO_PIN_4;			//PC4 = output low
	GPIOC->DDR |= (uint8_t)  GPIO_PIN_4;			//PC4 = output
	GPIOC->CR1 |= (uint8_t)  GPIO_PIN_4;			//PC4 = output push pull
	GPIOC->CR2 |= (uint8_t)  GPIO_PIN_4;			//PC4 = output high speed
	
	
	
	
	//SOFT_SPI_CLK
	GPIOD->ODR &= (uint8_t) ~GPIO_PIN_2;			//PD2 = output low
	GPIOD->DDR |= (uint8_t)  GPIO_PIN_2;			//PD2 = output
	GPIOD->CR1 |= (uint8_t)  GPIO_PIN_2;			//PD2 = output push pull
	GPIOD->CR2 |= (uint8_t)  GPIO_PIN_2;			//PD2 = output high speed
	
	//SOFT_SPI_MOSI
	GPIOD->ODR &= (uint8_t) ~GPIO_PIN_3;			//PD3 = output low
	GPIOD->DDR |= (uint8_t)  GPIO_PIN_3;			//PD3 = output
	GPIOD->CR1 |= (uint8_t)  GPIO_PIN_3;			//PD3 = output push pull
	GPIOD->CR2 |= (uint8_t)  GPIO_PIN_3;			//PD3 = output high speed
	
	//SOFT_SPI_CE
	GPIOD->ODR &= (uint8_t) ~GPIO_PIN_4;			//PD4 = output low
	GPIOD->DDR |= (uint8_t)  GPIO_PIN_4;			//PD4 = output
	GPIOD->CR1 |= (uint8_t)  GPIO_PIN_4;			//PD4 = output push pull
	GPIOD->CR2 &= (uint8_t) ~GPIO_PIN_4;			//PD4 = output low speed
	
	//SOFT_SPI_A0
	GPIOD->ODR &= (uint8_t) ~GPIO_PIN_5;			//PD5 = output low
	GPIOD->DDR |= (uint8_t)  GPIO_PIN_5;			//PD5 = output
	GPIOD->CR1 |= (uint8_t)  GPIO_PIN_5;			//PD5 = output push pull
	GPIOD->CR2 &= (uint8_t) ~GPIO_PIN_5;			//PD5 = output low speed
	
	//SOFT_SPI_RESET
	GPIOD->ODR &= (uint8_t) ~GPIO_PIN_6;			//PD6 = output low
	GPIOD->DDR |= (uint8_t)  GPIO_PIN_6;			//PD6 = output
	GPIOD->CR1 |= (uint8_t)  GPIO_PIN_6;			//PD6 = output push pull
	GPIOD->CR2 &= (uint8_t) ~GPIO_PIN_6;			//PD6 = output low speed
	
	
	
	//LED
	//GPIOC->ODR &= (uint8_t) ~GPIO_PIN_3;			//PC3 = output low
	//GPIOC->DDR |= (uint8_t)  GPIO_PIN_3;			//PC3 = output
	//GPIOC->CR1 |= (uint8_t)  GPIO_PIN_3;			//PC3 = output push pull
	//GPIOC->CR2 &= (uint8_t) ~GPIO_PIN_3;			//PC3 = output low speed

}