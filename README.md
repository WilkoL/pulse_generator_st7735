# pulse_generator_st7735

Pulse generator with STM8S103. Creates pulses with frequency from 0.01 Hz up to 2 MHz
and pulse lengths from 250ns up to 90s